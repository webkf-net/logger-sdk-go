package logger

import (
	"errors"
	"fmt"
	"path"
	"runtime"
	"strings"
)

//自定义一个日志库
type LogLevel uint16

//Logger接口
type Logger interface {
	Debug(format string, a ...interface{})
	Trace(format string, a ...interface{})
	Info(format string, a ...interface{})
	Warning(format string, a ...interface{})
	Error(format string, a ...interface{})
	Fatal(format string, a ...interface{})
}

//定义日志级别
const (
	UNKNOWN LogLevel = iota
	DEBUG
	TRACE
	INFO
	WARNING
	ERROR
	FATAL
)

//解析日志级别
func parseLogLevel(s string) (LogLevel, error) {
	s = strings.ToLower(s) //转为小写字母
	switch s {
	case "debug":
		return DEBUG, nil
	case "trace":
		return TRACE, nil
	case "info":
		return INFO, nil
	case "warning":
		return WARNING, nil
	case "error":
		return ERROR, nil
	case "fatal":
		return FATAL, nil
	default:
		err := errors.New("无效的日志级别")
		return UNKNOWN, err
	}
}
func getLogString(lv LogLevel) string {
	switch lv {
	case DEBUG:
		return "DEBUG"
	case TRACE:
		return "TRACE"
	case INFO:
		return "INFO"
	case WARNING:
		return "WARNING"
	case ERROR:
		return "ERROR"
	case FATAL:
		return "FATAL"
	}
	return "DEBUG"
}

//获取当前行相关信息
func getInfo(skip int) (funcName, fileName string, lineNo int) {
	pc, file, lineNo, ok := runtime.Caller(skip)
	if !ok {
		fmt.Printf("reuntime.Caller() failed\n")
		return
	}
	funcName = runtime.FuncForPC(pc).Name()
	fileName = path.Base(file)
	funcName = strings.Split(funcName, ".")[1] //用.分割后取第一个
	return
}

//判断日志保存类型
func NewLogger(logType, level, fileName, path string, maxSize int64) Logger {
	//日志级别，大于等于当前级别的日志会记录
	if level == "" {
		level = "info" //支持debug、trace、info、warning、error、fatal
	}
	if logType == "file" { //文件日志实例
		if fileName == "" { //默认文件名
			fileName = "log.log"
		}
		if path == "" { //默认保存到目录
			path = "./"
		}
		if maxSize == 0 {
			maxSize = 10 //默认每10M切割一个文件
		}
		maxSize *= 1024 * 1024
		return NewFileLogger(level, path, fileName, maxSize)
	} else { //终端日志实例
		return NewConsoleLogger(level)
	}
}
